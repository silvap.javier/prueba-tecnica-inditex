import { lazy } from "react";
import { Outlet, RouteObject } from "react-router-dom";
import { MainRoutes } from "./MainRouter.types";
import MainLayout from "../layout/MainLayout/MainLayout";

const HomePage = lazy(() => import("../pages/HomePage/HomePage"));
const PodcastPage = lazy(() => import("../pages/PodcastPage/PodcastPage"));
const EpisodePage = lazy(() => import("../pages/EpisodePage/EpisodePage"));

export const useMainRoutes = (): RouteObject[] => {
    return [
        {
            path: MainRoutes.MAIN,
            element: (
                <MainLayout>
                    <Outlet />
                </MainLayout>
            ),
            children: [
                {
                    path: MainRoutes.MAIN,
                    element: <HomePage />,
                },
                {
                    path: MainRoutes.VIEW_PODCAST,
                    element: <PodcastPage />,
                },
                {
                    path: MainRoutes.VIEW_EPISODE,
                    element: <EpisodePage />,
                },
            ],
        },
    ];
};
