export enum MainRoutes {
    MAIN = "/",
    VIEW_PODCAST = "/podcast/:id",
    VIEW_EPISODE = "/podcast/:podcastId/episode/:episodeId",
}
