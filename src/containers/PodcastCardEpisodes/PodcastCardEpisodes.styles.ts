import { Col } from "react-bootstrap";
import { css, styled } from "styled-components";

export const PodcastCardEpisodesRoot = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 1.5rem;
    `,
);