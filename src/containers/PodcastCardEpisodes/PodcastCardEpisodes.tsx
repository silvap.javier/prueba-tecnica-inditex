import { PodcastCardEpisodesProps } from "./PodcastCardEpisodes.types";
import useGetEpisodes from "../../hooks/useGetEpisodes";
import PodcastCardEpisodesCount from "../PodcastCardEpisodesCount/PodcastCardEpisodesCount";
import { PodcastCardEpisodesRoot } from "./PodcastCardEpisodes.styles";
import { getRowsEpisodesList } from "../../utils/getRowsEpisodesList";
import PodcastCardEpisodesTable from "../PodcastCardEpisodesTable/PodcastCardEpisodesTable";
import { useLoadingStore } from "../../store/loadingStore";
import { useEffect } from "react";

function PodcastCardEpisodes({ id }: PodcastCardEpisodesProps): JSX.Element {
    const setIsLoading = useLoadingStore((state) => state.setIsLoading);
    const { data, isFetching } = useGetEpisodes(id);
    const episodes = getRowsEpisodesList(data?.results || []);

    useEffect(() => {
        if (!isFetching) {
            setIsLoading(false);
        } else {
            setIsLoading(true);
        }
    }, [data, isFetching]);

    return (
        <PodcastCardEpisodesRoot>
            <PodcastCardEpisodesCount count={episodes.length || 0} />
            <PodcastCardEpisodesTable episodes={data?.results || []} podcastId={id} />
        </PodcastCardEpisodesRoot>
    );
}

export default PodcastCardEpisodes;
