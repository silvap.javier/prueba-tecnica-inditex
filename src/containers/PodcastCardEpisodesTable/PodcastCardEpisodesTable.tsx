import { PodcastCardEpisodesTableProps } from "./PodcastCardEpisodesTable.types";
import { getHeadersEpisodesList } from "../../utils/getHeadersEpisodesList";
import { getRowsEpisodesList } from "../../utils/getRowsEpisodesList";
import { Card } from "react-bootstrap";
import EpisodesTable from "../EpisodesTable/EpisodesTable";

function PodcastCardEpisodesTable({ episodes, podcastId }: PodcastCardEpisodesTableProps): JSX.Element {
    const headers = getHeadersEpisodesList();
    const rows = getRowsEpisodesList(episodes);
    return (
        <Card>
            <Card.Body>
                <EpisodesTable headers={headers} rows={rows} podcastId={podcastId} />
            </Card.Body>
        </Card>
    );
}

export default PodcastCardEpisodesTable;
