import { css, styled } from "styled-components";
import { Table as BaseTable } from "react-bootstrap";

export const EpisodesTableRoot = styled(BaseTable)(
    ({theme}) => css`
       a {
            color: ${theme.palette.primary};
        }
    `,
);