import { TableProps as  BaseTableProps} from "react-bootstrap";
import { EpisodeInfo } from "../../utils/getRowsEpisodesList";

export interface EpisodesTableProps extends BaseTableProps {
    className?: string;
    style?: React.CSSProperties;
    headers: string[];
    rows: EpisodeInfo[];
    podcastId: number;
}