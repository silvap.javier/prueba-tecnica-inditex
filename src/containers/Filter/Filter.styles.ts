import { Row } from "react-bootstrap";
import { css, styled } from "styled-components";

export const FilterRoot = styled(Row)(
    () => css`
        justify-content: flex-end;
    `,
);

export const FilterWrapper = styled(Row)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["2"]};
        width: 30rem;
        justify-content: space-between;
        align-items: center;
    `,
);