import { Card } from "react-bootstrap";
import { CardBody, CardImage } from "./PodcastCard.styles";
import { PodcastCardProps } from "./PodcastCard.types";
import { useNavigate } from "react-router-dom";
import { MainRoutes } from "../../router/MainRouter.types";
import getInfoPodcast from "../../utils/getInfoPodcast";

function PodcastCard({ podcast }: PodcastCardProps): JSX.Element {
    const navigate = useNavigate();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [image, title, author, description, id] = getInfoPodcast(podcast);
    return (
        <Card
            style={{ width: "18rem", cursor: "pointer" }}
            className="shadow"
            onClick={() => navigate(MainRoutes.VIEW_PODCAST.replace(":id", String(id)))}
        >
            <CardImage variant="" src={image} />
            <CardBody>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{`Author : ${author}`}</Card.Text>
            </CardBody>
        </Card>
    );
}

export default PodcastCard;
