import { Card as BaseCard} from "react-bootstrap";
import { css, styled } from "styled-components";

export const CardImage = styled(BaseCard.Img)(
    () => css`
        position: absolute;
        width: 10rem;
        height: 10rem;
        border-radius: 50%;
        top: -5rem;
        left: 4rem;
    `,
);

export const CardBody = styled(BaseCard.Body)(
    () => css`
        padding-top: 6rem;
        text-align: center;
    `,
);

