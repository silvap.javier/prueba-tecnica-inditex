import { Entry } from "../../types/podcasts.types";

export type PodcastCardProps = {
    podcast: Entry;
};