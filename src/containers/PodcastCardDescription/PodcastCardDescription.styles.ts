import { Card as BaseCard, Col} from "react-bootstrap";
import { css, styled } from "styled-components";

export const CardImage = styled(BaseCard.Img)(
    () => css`
    width: 14rem;
    height: 14rem;
    margin: 2rem auto;
    cursor: pointer;
    `,
);

export const CardBody = styled(BaseCard.Body)(
    () => css`
        text-align: left;
        border-top: 1px solid #e0e0e0;
        padding: 2rem 0rem 2rem 2remrem;
        cursor: pointer;
    `,
);

export const CardFooter = styled(BaseCard.Footer)(
    () => css`
        text-align: left;
        border-top: 1px solid #e0e0e0;
        gap-column: 1rem;
        background-color: transparent;
        padding-top: 1.5rem;
    `,
);



export const CardFooterContent = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 0.5rem;
    `,
);