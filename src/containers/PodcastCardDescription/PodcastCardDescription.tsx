import { Card } from "react-bootstrap";
import { PodcastCardDescriptionProps } from "./PodcastCardDescription.types";
import { CardBody, CardFooter, CardFooterContent, CardImage } from "./PodcastCardDescription.styles";
import getInfoPodcast from "../../utils/getInfoPodcast";
import Typography from "../../components/Typography/Typography";
import { useNavigate } from "react-router-dom";
import { MainRoutes } from "../../router/MainRouter.types";

function PodcastCardDescription({ podcast }: PodcastCardDescriptionProps): JSX.Element {
    const navigate = useNavigate();
    const [image, title, author, description, id] = getInfoPodcast(podcast);
    const lines = description.split("\n");

    return (
        <Card style={{ width: "20rem", height: "fit-content" }} className="shadow">
            <CardImage src={image} onClick={() => navigate(MainRoutes.VIEW_PODCAST.replace(":id", String(id)))} />
            <CardBody onClick={() => navigate(MainRoutes.VIEW_PODCAST.replace(":id", String(id)))}>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{`By ${author}`}</Card.Text>
            </CardBody>
            <CardFooter>
                <CardFooterContent>
                    <Typography variant="h5">Description : </Typography>
                    <div>
                        {lines.map((line, index) => (
                            <p key={index} dangerouslySetInnerHTML={{ __html: line }} />
                        ))}
                    </div>
                </CardFooterContent>
            </CardFooter>
        </Card>
    );
}

export default PodcastCardDescription;
