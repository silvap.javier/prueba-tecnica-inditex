import { Entry } from "../../types/podcasts.types";

export type PodcastCardDescriptionProps = {
    podcast?: Entry;
};