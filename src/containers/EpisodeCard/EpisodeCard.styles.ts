import { Card, Col } from "react-bootstrap";
import { css, styled } from "styled-components";

export const EpisodeCardRoot = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 1.5rem;
    `,
);

export const EpisodeCardComponent = styled(Card)(
    () => css`
        padding: 1rem;
    `,
);