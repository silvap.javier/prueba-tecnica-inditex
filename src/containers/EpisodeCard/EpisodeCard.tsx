import { Card } from "react-bootstrap";
import { EpisodeCardProps } from "./EpisodeCard.types";
import { useState } from "react";
import useGetEpisodes from "../../hooks/useGetEpisodes";
import { EpisodeCardComponent, EpisodeCardRoot } from "./EpisodeCard.styles";

function EpisodeCard({ podcastId, episodeId }: EpisodeCardProps): JSX.Element {
    const { data: episodes } = useGetEpisodes(podcastId);

    const episode = episodes?.results.find((episode) => episode.trackId === episodeId);
    const [isPlaying, setIsPlaying] = useState(false);

    const togglePlay = () => {
        setIsPlaying(!isPlaying);
    };

    return (
        <EpisodeCardRoot>
            <EpisodeCardComponent className="shadow">
                <Card.Body>
                    <Card.Title>{episode?.trackName}</Card.Title>
                    <Card.Text>{episode?.description}</Card.Text>
                </Card.Body>
                <Card.Footer>
                    <audio controls={true} autoPlay={false} onPlay={togglePlay} onPause={togglePlay}>
                        <source src={episode?.episodeUrl} type="audio/mpeg" />
                        Your browser does not support the audio element.
                    </audio>
                </Card.Footer>
            </EpisodeCardComponent>
        </EpisodeCardRoot>
    );
}

export default EpisodeCard;
