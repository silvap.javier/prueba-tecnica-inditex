import { Card } from "react-bootstrap";
import Typography from "../../components/Typography/Typography";
import { PodcastCardEpisodesCountProps } from "./PodcastCardEpisodesCount.types";

function PodcastCardEpisodesCount({ count }: PodcastCardEpisodesCountProps): JSX.Element {
    return (
        <Card className="shadow">
            <Card.Body>
                <Card.Title>
                    <Typography variant="h4">{`Episodes : ${count}`}</Typography>
                </Card.Title>
            </Card.Body>
        </Card>
    );
}

export default PodcastCardEpisodesCount;
