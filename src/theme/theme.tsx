// theme.ts

const theme = {
    palette: {
        primary: "#526d89",
        secondary: "#6c757d",
        success: "#28a745",
        info: "#17a2b8",
        warning: "#ffc107",
        danger: "#dc3545",
        light: "#f8f9fa",
        dark: "#343a40",
        background: "#f8f9fa",
        border: "#cecece",
    },
    fonts: {
        body: "'Arial', sans-serif",
        heading: "'Helvetica', sans-serif",
    },
    fontSizes: {
        small: "0.875rem",
        base: "1rem",
        large: "1.25rem",
    },
    borderRadius: "0.5rem",
    spacing: {
        0: "0", // 0px
        1: "0.25rem", // 4px
        1.5: "0.375rem", // 6px
        2: "0.5rem", // 8px
        2.5: "0.625rem", // 10px
        3: "0.75rem", // 12px
        3.5: "0.875rem", // 14px
        4: "1rem", // 16px
        4.5: "1.125rem", // 18px
        5: "1.25rem", // 20px
        6: "1.5rem", // 24px
        7: "1.75rem", // 28px
        8: "2rem", // 32px
        10: "2.5rem", // 40px
        12: "3rem", // 48px
        12.5: "3.125rem", // 50px
        15: "3.75rem", // 60px
        16: "4rem", // 64px
        20: "5rem", // 80px
        22: "5.25rem", // 84px
    },
};

export default theme;
