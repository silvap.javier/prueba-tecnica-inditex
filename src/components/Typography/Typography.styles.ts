import styled, { css } from "styled-components";

export const TypographyRoot = styled("div")(
    () => css`
    &.h1 {
        font-size: 2.5rem;
        font-weight: bold;
        margin-bottom: 1rem;
        color: ${({ theme }) => theme.palette.primary};
      }
    
      &.h2 {
        font-size: 2rem;
        font-weight: bold;
        margin-bottom: 0.75rem;
        color: ${({ theme }) => theme.palette.primary};
      }
    
      &.h3 {
        font-size: 1.75rem;
        font-weight: bold;
        margin-bottom: 0.5rem;
      }
    
      &.h4 {
        font-size: 1.5rem;
        font-weight: bold;
        margin-bottom: 0.5rem;
      }
    
      &.h5 {
        font-size: 1.15rem;
        font-weight: bold;
        margin-bottom: 0.25rem;
      }
    
      &.h6 {
        font-size: 1rem;
        font-weight: 400;
        margin-bottom: 0.25rem;
      }
    
      &.p {
        font-size: 1rem;
        margin-bottom: 0.5rem;
      }
    `,
);