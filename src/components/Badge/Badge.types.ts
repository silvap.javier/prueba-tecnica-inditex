import { BadgeProps as  BaseBadgeProps} from "react-bootstrap";

export interface BadgeProps extends BaseBadgeProps {
    className?: string;
    style?: React.CSSProperties;
    value: number | string;
}