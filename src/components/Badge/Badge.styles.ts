import { Badge } from "react-bootstrap";
import { css, styled } from "styled-components";

export const BadgeRoot = styled(Badge)(
    ({ theme }) => css`
        background-color: ${theme.palette.primary};
        color: ${theme.palette.white};
        padding: ${theme.spacing["1"]};
        border-radius: ${theme.borderRadius};
        font-size: ${theme.fontSizes["base"]};
        width: 3rem;
        height: 1.5rem;
    `,
);