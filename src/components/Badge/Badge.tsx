import clsx from "clsx";
import { BadgeRoot } from "./Badge.styles";
import { BadgeProps } from "./Badge.types";

function Badge({ className, value }: BadgeProps): JSX.Element {
    return <BadgeRoot className={clsx("badge", className)}>{value}</BadgeRoot>;
}

export default Badge;
