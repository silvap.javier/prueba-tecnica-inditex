import { Toast as BaseToast } from "react-bootstrap";
import { styled } from "styled-components";

export const ToastRoot = styled(BaseToast)`
  position: absolute;
  top: 0;
  right: 1rem;
`;