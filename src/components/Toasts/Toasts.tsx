import { Toast } from "react-bootstrap";
import { ToastProps } from "./Toasts.types";
import Typography from "../Typography/Typography";
import { ToastRoot } from "./Toasts.styles";
import { useState } from "react";

function Toasts({ title, message, show, ...rest }: ToastProps): JSX.Element {
    const [showToast, setShowToast] = useState(show);
    return (
        <ToastRoot show={showToast} onClose={() => setShowToast(false)} {...rest}>
            <Toast.Header>
                <Typography variant="h6">{title}</Typography>
            </Toast.Header>
            <Toast.Body>{message}</Toast.Body>
        </ToastRoot>
    );
}

export default Toasts;
