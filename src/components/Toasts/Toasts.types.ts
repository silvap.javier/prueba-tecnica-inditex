import { ToastProps as ToastPropsProps} from "react-bootstrap";

export interface ToastProps extends ToastPropsProps {
    title: string;
    message?: string;
}