export type GridProps<T> = {
    items: T[]; // Elementos a renderizar
    renderItem: (item: T) => React.ReactNode; // Función para renderizar cada elemento
    count: number; // Número de elementos a renderizar
};