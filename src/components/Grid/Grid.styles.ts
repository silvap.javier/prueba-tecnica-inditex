import { Container } from "react-bootstrap";
import { styled } from "styled-components";

export const GridContainer = styled(Container)`
    padding-top: 20px;
`;