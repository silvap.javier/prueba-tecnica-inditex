import { Container, InputGroup } from "react-bootstrap";
import { styled } from "styled-components";

export const SearchContainer = styled(Container)`
  width: 24rem;
  padding: 0;
`;

export const StyledInputGroup = styled(InputGroup)`
  flex: 1;
`;