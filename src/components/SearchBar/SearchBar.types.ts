import { FormControlProps } from "react-bootstrap";

export interface SearchBarProps extends Omit<FormControlProps, "prefix" | "defaultValue"> {
    placeholder?: string;
    onSearch: (value: string) => void;
    defaultValue?: string;
}
