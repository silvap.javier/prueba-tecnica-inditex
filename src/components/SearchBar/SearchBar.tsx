import React, { useState } from "react";
import { FormControl } from "react-bootstrap";
import { SearchBarProps } from "./SearchBar.types";
import { SearchContainer, StyledInputGroup } from "./SearchBar.styles";
import _ from "lodash";

function SearchBar({ onSearch, defaultValue, placeholder }: SearchBarProps): JSX.Element {
    const [query, setQuery] = useState(defaultValue);

    const debouncedSearch = _.debounce((searchQuery: string) => {
        onSearch(searchQuery);
    }, 500);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newQuery = event.target.value;
        setQuery(newQuery);
        debouncedSearch(newQuery);
    };

    const handleSearch = () => {
        if (query) {
            onSearch(query);
        }
    };

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleSearch();
        }
    };

    return (
        <SearchContainer>
            <StyledInputGroup>
                <FormControl placeholder={placeholder} value={query} onChange={handleInputChange} onKeyPress={handleKeyPress} />
            </StyledInputGroup>
        </SearchContainer>
    );
}

export default SearchBar;
