import styled, { css } from "styled-components";
import { Col } from "react-bootstrap";
import DescriptionPage from "../../layout/DescriptionPage/DescriptionPage";

export const PodcastPageRoot = styled(DescriptionPage)(
    () => css`
    `,
);

export const HomePageWrapper = styled(Col)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["7"]};
        flex: 1;
    `,
);