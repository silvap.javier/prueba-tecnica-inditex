import clsx from "clsx";
import { HomePageRoot, HomePageWrapper } from "./HomePage.styles";
import { HomePageProps } from "./HomePage.types";
import Filter from "../../containers/Filter/Filter";
import PodcastGrid from "../../containers/PodcastGrid/PodcastGrid";
import useGetPodcasts from "../../hooks/useGetPodcasts";
import { useEffect, useState } from "react";
import { useLoadingStore } from "../../store/loadingStore";

function HomePage({ className, ...rest }: HomePageProps): JSX.Element {
    const { data, isFetching, isError } = useGetPodcasts();
    const [searhTerm, setSearchTerm] = useState<string>("");
    const [filteredData, setFilteredData] = useState<any>([]);
    const setIsLoading = useLoadingStore((state) => state.setIsLoading);

    useEffect(() => {
        if (!isFetching) {
            setIsLoading(false);
        } else {
            setIsLoading(true);
        }
    }, [data, isFetching]);

    useEffect(() => {
        if (data) {
            const filteredData = data.feed.entry.filter((podcast: any) => {
                return (
                    podcast["im:name"].label.toLowerCase().includes(searhTerm.toLowerCase()) ||
                    podcast["im:artist"].label.toLowerCase().includes(searhTerm.toLowerCase())
                );
            });
            setFilteredData(filteredData);
        }
    }, [data, searhTerm]);

    return (
        <HomePageRoot className={clsx("home-page", className)} {...rest}>
            <HomePageWrapper>
                <Filter searhTerm={searhTerm} setSearchTerm={setSearchTerm} count={filteredData.length || 0} />
                <PodcastGrid isError={isError} podcasts={filteredData} />
            </HomePageWrapper>
        </HomePageRoot>
    );
}

export default HomePage;
