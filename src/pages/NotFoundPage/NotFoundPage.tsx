import { Link } from "react-router-dom";
import { NotFound404Text, NotFoundPageRoot, NotFoundText } from "./NotFoundPage.styles";
import { Button, Col } from "react-bootstrap";
import { MainRoutes } from "../../router/MainRouter.types";

const NotFoundPage = (): JSX.Element => {
    return (
        <NotFoundPageRoot>
            <NotFound404Text variant="h1">404</NotFound404Text>
            <Col alignItems="center" gap="0.5rem">
                <NotFoundText variant="h2">{"Not Found"}</NotFoundText>
                <NotFoundText variant="h3">{"The content you are looking for does not exist"}</NotFoundText>
            </Col>
            <Link to={MainRoutes.MAIN}>
                <Button>{"Go back home"}</Button>
            </Link>
        </NotFoundPageRoot>
    );
};

export default NotFoundPage;
