export interface IsLoggedInProps {
    isLoggedIn: boolean;
}
