import { useQuery } from '@tanstack/react-query';
import { PodcastsApiResponse } from '../types/podcasts.types';


const fetchPodcasts = async () => {
  const response = await fetch("https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json");
  return response.json();
};

const useGetPodcasts = () => {
  return  useQuery<PodcastsApiResponse>({ queryKey: ['getPodcasts'], queryFn: fetchPodcasts })
};

export default useGetPodcasts;
