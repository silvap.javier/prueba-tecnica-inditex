import { useQuery } from '@tanstack/react-query';
import { Entry } from '../types/podcasts.types';


const fetchPodcastById = async (id:number) => {
        const response = await fetch("https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json");
        const data = await response.json();
        return data.feed.entry.find((podcast:Entry) => podcast.id.attributes['im:id'] === String(id));
};

const useGetPodcast = (id:number) => {
    return  useQuery<Entry>({ queryKey: ['getPodcast',id], queryFn: () => fetchPodcastById(id) })
};

export default useGetPodcast;
