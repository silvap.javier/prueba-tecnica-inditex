/* eslint-disable @typescript-eslint/no-unused-vars */
import styled, { css } from "styled-components";

export const BaseLayoutRoot = styled.main(
    ({ theme }) => css`
        flex: 1;
        display: flex;
    `,
);
