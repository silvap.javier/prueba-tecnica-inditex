import { PropsWithChildren } from "react";
import BaseLayout from "../BaseLayout/BaseLayout";
import Header from "../../navigation/Header/Header";
import { Col } from "react-bootstrap";

const MainLayout = ({ children }: PropsWithChildren<{}>): JSX.Element => (
    <BaseLayout>
        <Col>
            <Header title="Podcaster" />
            {children}
        </Col>
    </BaseLayout>
);

export default MainLayout;
