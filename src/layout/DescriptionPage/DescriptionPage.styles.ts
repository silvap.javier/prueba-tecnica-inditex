import { Row } from "react-bootstrap";
import styled, { css } from "styled-components";

export const DescriptionPageRoot = styled.div(
    () => css`
        max-width: 100%;
        padding: 25px;
        flex: 1;
        display: flex;
        flex-direction: column;
    `,
);


export const DescriptionPageRow = styled(Row)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["7"]};
        flex: 1;
        flex-direction: row;
    `,
);
