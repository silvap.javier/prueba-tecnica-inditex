import { Suspense } from "react";
import Router from "./router/Router";
import ConfigProvider from "./theme/ConfigProvider";

const App = (): JSX.Element | null => {
    return (
        <Suspense fallback={<></>}>
            <ConfigProvider>
                <Router />
            </ConfigProvider>
        </Suspense>
    );
};

export default App;
