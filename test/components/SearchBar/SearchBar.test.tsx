/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { fireEvent, render } from "@testing-library/react";
import SearchBar from "../../../src/components/SearchBar/SearchBar";

describe("SearchBar component", () => {
    test("renders correctly with placeholder and default value", () => {
        const placeholder = "Search";
        const defaultValue = "Initial Value";

        const { getByPlaceholderText, getByDisplayValue } = render(
            <SearchBar placeholder={placeholder} defaultValue={defaultValue} onSearch={() => {}} />,
        );

        expect(getByPlaceholderText(placeholder)).toBeInTheDocument();
        expect(getByDisplayValue(defaultValue)).toBeInTheDocument();
    });

    test("triggers onSearch callback when Enter key is pressed", () => {
        const placeholder = "Search";
        const defaultValue = "Initial Value";
        const searchValue = "New Value";

        const mockSearchHandler = jest.fn();

        const { getByRole } = render(<SearchBar placeholder={placeholder} defaultValue={defaultValue} onSearch={mockSearchHandler} />);
        const inputElement = getByRole("textbox");

        fireEvent.keyPress(inputElement, { key: "Enter", code: 13, charCode: 13 });
        expect(mockSearchHandler).toHaveBeenCalledWith(defaultValue);
    });
});
