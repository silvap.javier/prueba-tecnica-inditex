import { render } from "@testing-library/react";
import Badge from "../../../src/components/Badge/Badge";
import ConfigProvider from "../../../src/theme/ConfigProvider";

describe("Badge component", () => {
    test("renders with value", () => {
        const screen = render(
            <ConfigProvider>
                <Badge value="3" />
            </ConfigProvider>,
        );
        expect(screen.getByText("3")).toBeInTheDocument();
    });

    test("renders with custom className", () => {
        const screen = render(
            <ConfigProvider>
                <Badge value="3" className="custom-class" />
            </ConfigProvider>,
        );
        expect(screen.getByText("3")).toHaveClass("custom-class");
    });
});
