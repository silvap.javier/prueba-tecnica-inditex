/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { render } from "@testing-library/react";
import Toasts from "../../../src/components/Toasts/Toasts";
import ConfigProvider from "../../../src/theme/ConfigProvider";

describe("Toasts component", () => {
    test("renders correctly with title and message", () => {
        // Define los datos necesarios para la prueba
        const title = "Title";
        const message = "Message";
        const show = true;

        // Renderiza el componente
        const { getByText } = render(
            <ConfigProvider>
                <Toasts title={title} message={message} show={show} />
            </ConfigProvider>,
        );

        // Verifica que el componente se haya renderizado correctamente con el título y el mensaje
        expect(getByText(title)).toBeInTheDocument();
        expect(getByText(message)).toBeInTheDocument();
    });
});
