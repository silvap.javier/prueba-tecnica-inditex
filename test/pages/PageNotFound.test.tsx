// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { render, screen } from "@testing-library/react";
import NotFoundPage from "../../src/pages/NotFoundPage/NotFoundPage";
import { BrowserRouter } from "react-router-dom";
import ConfigProvider from "../../src/theme/ConfigProvider";

describe("NotFoundPage", () => {
    test("renders correctly", () => {
        render(
            <BrowserRouter>
                <ConfigProvider>
                    <NotFoundPage />
                </ConfigProvider>
            </BrowserRouter>,
        );

        expect(screen.getByText("404")).toBeInTheDocument();
        expect(screen.getByText("Not Found")).toBeInTheDocument();
        expect(screen.getByText("The content you are looking for does not exist")).toBeInTheDocument();
        const goBackHomeButton = screen.getByRole("button", { name: "Go back home" });
        expect(goBackHomeButton).toBeInTheDocument();
    });
});
