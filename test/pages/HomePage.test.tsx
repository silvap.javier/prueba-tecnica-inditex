// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { act, render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import HomePage from "../../src/pages/HomePage/HomePage";
import * as useGetPodcastsModule from "../../src/hooks/useGetPodcasts";
import { PodcastsMock } from "../mock/Podcasts.mock";
import { UseQueryResult } from "@tanstack/react-query";
import { PodcastsApiResponse } from "../../src/types/podcasts.types";
import ConfigProvider from "../../src/theme/ConfigProvider";

describe("HomePage", () => {
    let useGetPodcastsMock: jest.SpyInstance;

    beforeEach(() => {
        // Mockea la respuesta simulada
        useGetPodcastsMock = jest.spyOn(useGetPodcastsModule, "default").mockReturnValue({
            data: PodcastsMock,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<PodcastsApiResponse, Error>);
    });

    afterEach(() => {
        // Restaura la implementación original de los hooks
        useGetPodcastsMock.mockRestore();
    });

    test("renders HomePage correctly", async () => {
        await act(async () => {
            render(
                <Router>
                    <ConfigProvider>
                        <HomePage />
                    </ConfigProvider>
                </Router>,
            );
        });

        expect(screen.getByText(PodcastsMock.feed.entry[0]["im:name"].label)).toBeInTheDocument();
        expect(
            screen.getByText((_, element) => {
                // Función de comparación de texto personalizada que coincide con el texto "Author: ArtistName"
                // independientemente de los elementos circundantes
                const normalizedText = element?.textContent?.replace(/\s/g, "");
                return normalizedText === "Author:ArtistName";
            }),
        ).toBeInTheDocument();
    });

    test("calls useGetPodcasts hook with correct parameters", async () => {
        await act(async () => {
            render(
                <Router>
                    <ConfigProvider>
                        <HomePage />
                    </ConfigProvider>
                </Router>,
            );
        });
        expect(useGetPodcastsMock).toBeCalledTimes(2);
    });
});
