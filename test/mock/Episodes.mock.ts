import { EpisodesApiResponse } from "../../src/types/episodes.types";


export const episodesApiResponseMock: EpisodesApiResponse = {
    resultCount: 1,
    results: [
        {
            wrapperType: "podcastEpisode",
            kind: "podcast-episode",
            collectionId: 123456,
            trackId: 789012,
            collectionName: "The Joe Budden Podcast",
            trackName: "Episode 1",
            artistViewUrl: "https://example.com/artist",
            collectionViewUrl: "https://example.com/collection",
            feedUrl: "https://example.com/feed",
            trackViewUrl: "https://example.com/track",
            artworkUrl60: "https://example.com/artwork60",
            releaseDate: new Date(),
            contentAdvisoryRating: "Explicit",
            artworkUrl600: "https://example.com/artwork600",
            genres: ["Music"],
            artworkUrl160: "https://example.com/artwork160",
            episodeContentType: "audio",
            closedCaptioning: "none",
            shortDescription: "Episode description",
            description: "Episode full description",
            trackTimeMillis: 0, 
            country: "USA",
        },
    ],
};
