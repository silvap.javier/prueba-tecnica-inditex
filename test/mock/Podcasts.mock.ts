import { PodcastsApiResponse } from "../../src/types/podcasts.types";

export const PodcastsMock: PodcastsApiResponse = {
    feed: {
      author: {
        name: { label: 'Author Name' },
        uri: { label: 'https://example.com/author' },
      },
      entry: [
        {
          'im:name': { label: 'Podcast Name' },
          'im:image': [{ label: 'Image URL', attributes: { height: '100' } }],
          summary: { label: 'Podcast summary' },
          'im:price': { label: 'Price', attributes: { amount: '9.99', currency: 'USD' } },
          'im:contentType': { attributes: { term: 'Term', label: 'Content Type' } },
          rights: { label: 'Rights' },
          title: { label: 'Podcast Title' },
          link: { attributes: { rel: 'rel', type: 'type', href: 'https://example.com/podcast' } },
          id: { label: 'ID', attributes: { 'im:id': '123456' } },
          'im:artist': { label: 'Artist Name', attributes: { href: 'https://example.com/artist' } },
          category: { attributes: { 'im:id': '789', term: 'Term', scheme: 'Scheme', label: 'Category' } },
          'im:releaseDate': { label: 'Release Date', attributes: { label: 'Date' } },
        },
      ],
      updated: { label: 'Updated Date' },
      rights: { label: 'Rights' },
      title: { label: 'Feed Title' },
      icon: { label: 'Icon URL' },
      link: [{ attributes: { rel: 'rel', type: 'type', href: 'https://example.com/link' } }],
      id: { label: 'Feed ID' },
    },
  };
  