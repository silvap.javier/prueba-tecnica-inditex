/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  moduleDirectories: ["src", "node_modules"],
  transform: {
      "\\.[jt]sx?$": "ts-jest",
      "node_modules/variables/.+\\.(j|t)sx?$": "ts-jest",
  },
  transformIgnorePatterns: ["node_modules/(?!variables/.*)"],
  coveragePathIgnorePatterns: [
      "!*.d.ts",
      "!*styles.tsx",
      "src/hooks/*",
      "src/router/*",
      "src/utils/*",
  ],
  moduleNameMapper: {
      "\\.(css|less|sass)$": "identity-obj-proxy",
  },
  setupFilesAfterEnv: [
    "<rootDir>/src/setupTests.ts"
  ]
};