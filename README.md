# Prueba Técnica Senior Frontend

Proyecto creado con [Create React App](https://github.com/facebook/create-react-app).

## Técnologias empleadas

Para la implementacion de las solusion se utilizaron las siguientes técnologias :

-   react 18.2.0 - Framework de implementacion de SPA
-   typescript 5.4.5 - Para tipado del código
-   bootstrap 5.3.3 - Para implementacion de los components
-   styled-components 6.1.8 - Para customizacion de los components
-   react-router-dom 6.22.3 - Para la navegacion y enrutado
-   @tanstack/react-query 5.29.2 - Para el manejo de las peticiones fetch y cache
-   zustand 4.5.2 - Manejo de estados (En la aplicación el manejo del loading)
-   moment 2.30.1 - Manejo de fechas de los episodios y la duración del mismo
-   prettier 3.2.5 - Formateo y limpieza de codigo
-   eslint 8.50.0 - Analisis del codigo posibles errores
-   jest 29.7.0 - Desarrollo de Unit Tests y Component Tests
-   craco 7.1.0 - Para modificaciones avanzadas en la configuración de Webpack logrando la optimizacion de recursos en diferentes enviroments

## Scripts

Se crearon los siguientes scrips :

### `yarn start`

Corre la aplicacion de modo development en local : [http://localhost:3000](http://localhost:3000)

### `yarn test`

Lanzar test con jest visualizacion el coverage.

### `yarn lint:fix`

Correccion y deteccion de errores
